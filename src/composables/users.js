/** ABOUT THIS FILE
 *
 * @description
 * - This is the users composables wherein all of the functions and properties
 * that needed for the app shown here.
 */
import { ref } from "vue";
import axios from "axios";

let rows = ref([]); // list of users

/**------------------------------ USER DATA TO BE PROVIDED ------------------------------ */
// Personal and Contact Details
let name = ref("");
let username = ref("");
let email = ref("");
let phoneNumber = ref("");
let website = ref("");

// Address
let street = ref("");
let suite = ref("");
let city = ref("");
let zipCode = ref("");

// Company
let companyName = ref("");
let catchPhrase = ref("");
let baseService = ref("");
/**-------------------------------- END OF USER DATA TO BE PROVIDED -------------------------------- */

/** -------------------------------- UTILITY PROPERTIES -------------------------------- */
let isAddBtnLoading = ref(false); // Indicates the loading UI when adding or updating the user.
let deleteLoading = ref(false); // Indicates the loading UI when deleting a user.
let confirmation = ref(false); // Confirmation dialog for deleting the user data.
let dialog = ref(false); // Dialog that popups when the user adds or updates the data.
let form = ref(null); // Reference for the actual form.
let userID = ref(null); // User ID
let date = ref(null); // Date of the selected user data
let updateModal = ref(false); // Shows the update modal/dialog.
/** -------------------------------- END OF UTILITY PROPERTIES -------------------------------- */

/** -------------------------------- UTILITY FUNCTIONS -------------------------------- */

/**
 * function emptyForm()
 *
 * @description
 *  - Sets all the form fields to empty
 */
let emptyForm = () => {
  if (form.value) form.value.reset();

  date.value = null;
  userID.value = null;
  name.value = "";
  username.value = "";
  email.value = "";
  phoneNumber.value = "";
  website.value = "";
  street.value = "";
  suite.value = "";
  city.value = "";
  zipCode.value = "";
  companyName.value = "";
  catchPhrase.value = "";
  baseService.value = "";
};

/**
 * function setUserToUpdate(user)
 *
 * @description
 * - Sets the user data to the form fields
 *
 * @param user : Object
 * - An object that consists all of the user details.
 */
let setUserToUpdate = (user) => {
  date.value = user.date;
  userID.value = user.id;
  name.value = user.name;
  username.value = user.username;
  email.value = user.email;
  phoneNumber.value = user.phone;
  website.value = user.website;
  street.value = user.address.street;
  suite.value = user.address.suite;
  city.value = user.address.city;
  zipCode.value = user.address.zipcode;
  companyName.value = user.company.name;
  catchPhrase.value = user.company.catchPhrase;
  baseService.value = user.company.bs;
};
/** -------------------------------- END OF UTILITY FUNCTIONS -------------------------------- */

/**-------------------------------- MAIN FUNCTIONS OF THE APPLICATION -------------------------------- */
/**
 * function getUsers()
 *
 * @description
 * - Gets all the users in the server.
 */
let getUsers = () => {
  emptyForm();
  axios
    .get("http://localhost:3003/users/")
    .then((response) => {
      rows.value = response.data;
    })
    .catch((err) => console.error(err));
};

/** function addUser()
 *
 * @description
 * - Adds new user in the server.
 */
let addUser = () => {
  isAddBtnLoading.value = true;

  // Expand this to know what are the types of data.
  /** NEW USER DATA TO BE ADDED
   *
   * New User consist of these following data:
   *
   * date: Date
   * name: String
   * username: String
   * email: String
   * phone: String
   * website: String
   * address: Object -> street, suite, city, zipcode : Strings
   * company: Object -> name, catchPhrase, bs : Strings
   */
  let newUser = {
    date: Date.now(),
    name: name.value,
    username: username.value,
    email: email.value,
    phone: phoneNumber.value,
    website: website.value,
    address: {
      street: street.value,
      suite: suite.value,
      city: city.value,
      zipcode: zipCode.value,
    },
    company: {
      name: companyName.value,
      catchPhrase: catchPhrase.value,
      bs: baseService.value,
    },
  };

  // POST REQUEST
  axios
    .post("http://localhost:3003/users", newUser)
    .then((_response) => {
      isAddBtnLoading.value = false;
      dialog.value = true;
      emptyForm();
    })
    .catch((error) => console.log(error));
};

/**
 * function deleteUser()
 *
 * @description
 * - A function that removes the user from the list.
 *
 * @param id : String
 * - A unique id that identifies the specifed user to be deleted.
 */
let deleteUser = (id) => {
  // DELETE REQUEST
  axios
    .delete(`http://localhost:3003/users/${id}`)
    .then((response) => {
      if (response.status >= 200) {
        rows.value = rows.value.filter((user) => user.id !== id);
      }
      deleteLoading.value = false;
      confirmation.value = false;
    })
    .catch((error) => console.log(error));
};

/**
 * function updateUser()
 *
 * @description
 * - A function that updates the data of specified user.
 */
let updateUser = () => {
  isAddBtnLoading.value = true;

  let newUser = {
    date: date.value,
    name: name.value,
    username: username.value,
    email: email.value,
    phone: phoneNumber.value,
    website: website.value,
    address: {
      street: street.value,
      suite: suite.value,
      city: city.value,
      zipcode: zipCode.value,
    },
    company: {
      name: companyName.value,
      catchPhrase: catchPhrase.value,
      bs: baseService.value,
    },
  };

  // PUT REQUEST
  axios
    .put(`http://localhost:3003/users/${userID.value}`, newUser)
    .then((response) => {
      userID.value = null;
      isAddBtnLoading.value = false;
      emptyForm();

      updateModal.value = true;
      dialog.value = true;

      setTimeout(() => {
        window.location.href = "/";
      }, 2000);
    });
};

/**---------------------------------------- END OF MAIN FUNCTIONS -------------------------------- */

export {
  rows,
  getUsers,
  addUser,
  deleteUser,
  setUserToUpdate,
  updateUser,
  emptyForm,
  name,
  username,
  email,
  phoneNumber,
  website,
  street,
  suite,
  city,
  zipCode,
  companyName,
  catchPhrase,
  baseService,
  isAddBtnLoading,
  deleteLoading,
  confirmation,
  dialog,
  form,
  userID,
  updateModal,
};
