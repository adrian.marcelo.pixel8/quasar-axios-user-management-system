import { ref } from "vue";

let columns = ref([
  {
    name: "date",
    label: "Date Added",
    field: "date",
    align: "left",
    headerClasses: "header header--name",
    style: "font-size: 5rem",
    sortable: true,
    sort: (a, b, rowA, rowB) => parseInt(a, 10) - parseInt(b, 10),
    sortOrder: "ad",
  },
  {
    name: "name",
    label: "Name",
    field: "name",
    align: "left",
    headerClasses: "header header--name",
    style: "font-size: 5rem",
  },
  {
    name: "username",
    label: "Username",
    field: "username",
    align: "left",
    headerClasses: "header header--username",
  },
  {
    name: "email",
    label: "Email",
    field: "email",
    align: "left",
    headerClasses: "header header--email",
  },
  {
    name: "address",
    label: "Address",
    field: "address",
    align: "left",
    headerClasses: "header header--address",
  },
  {
    name: "phone",
    label: "Phone Number",
    field: "phone",
    align: "left",
    headerClasses: "header header--phone",
  },
  {
    name: "update",
    label: "Actions",
    field: "update",
    align: "left",
    headerClasses: "header header--update",
  },
  {
    name: "delete",
    label: "",
    field: "delete",
    align: "left",
    headerClasses: "header header--delete",
  },
  {
    name: "view",
    label: "",
    field: "view",
    align: "left",
    headerClasses: "header header--view",
  },
]);

export { columns };
